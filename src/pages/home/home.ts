import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userData: any;
  toastMessage: any;
  constructor(public navCtrl: NavController, private facebook : Facebook, private toast: ToastController) {

  }
  loginWithFB() {
    this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
      this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        this.userData = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'] }
      });
    });
  }
  logoutFB(){
    this.facebook.logout();
    this.navCtrl.setRoot(HomePage);
  }

  presentToast() {
    let toast = this.toast.create({
      message: this.toastMessage,
      duration: 3000
    });
    toast.present();
  }
}
